# gR(s)PC

A toy example showcasing a simple implementation of a gRPC server and client in
Rust.

The Rust bindings for the Protobuf contract are generated using
[buf](https://buf.build/). Install it before building by following the [install
instructions](https://buf.build/docs/installation) (or, if you are on an Arch
based system `buf` is available in the `extra` repository: `pacman -S buf`).
Bindings are then generated automatically at build time, see
[build.rs](./build.rs).

To see it all in action, first run the server with

```sh
cargo run --bin server
```

and when it is up and listening for requests, open another terminal and run the
client with

```sh
cargo run --bin client
```
