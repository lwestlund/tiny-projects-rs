use std::{
    error::Error,
    process::{exit, Command},
};

fn main() -> Result<(), Box<dyn Error>> {
    let proto_dir = concat!(env!("CARGO_MANIFEST_DIR"), "/proto");

    let status = Command::new("buf")
        .args(["generate"])
        .current_dir(proto_dir)
        .status()
        .unwrap();

    if !status.success() {
        exit(status.code().unwrap_or(-1))
    }

    println!("cargo:rerun-if-changed={}", proto_dir);

    Ok(())
}
