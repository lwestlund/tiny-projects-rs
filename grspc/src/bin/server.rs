use std::error::Error;

use grspc::contract::hello::v1::{
    greeter_service_server::{GreeterService, GreeterServiceServer},
    SayHelloRequest, SayHelloResponse,
};
use tonic::{transport::Server, Request, Response};

#[derive(Default)]
struct MyGreeterService {}

#[tonic::async_trait]
impl GreeterService for MyGreeterService {
    async fn say_hello(
        &self,
        request: Request<SayHelloRequest>,
    ) -> Result<Response<SayHelloResponse>, tonic::Status> {
        println!("Got a request from {:?}", request.remote_addr());

        let response = SayHelloResponse {
            message: format!("Hello {}!", request.into_inner().name),
        };

        Ok(Response::new(response))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let address = "[::]:50051".parse().unwrap();
    let greeter = MyGreeterService::default();

    println!("Greeter server listening on {address}");

    Server::builder()
        .add_service(GreeterServiceServer::new(greeter))
        .serve(address)
        .await?;

    Ok(())
}
