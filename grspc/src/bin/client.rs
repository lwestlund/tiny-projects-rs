use std::error::Error;

use grspc::contract::hello::v1::{greeter_service_client::GreeterServiceClient, SayHelloRequest};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut client = GreeterServiceClient::connect("http://[::]:50051").await?;

    let request = tonic::Request::new(SayHelloRequest {
        name: "Love".into(),
    });

    let response = client.say_hello(request).await?;

    println!("Response: {}", response.into_inner().message);

    Ok(())
}
