# R(edi)s

Store a Rust data structure in a local Redis database, read it back, and check
that it is the same data.

This assumes a Redis database running on your local machine. If you are on an
[Arch](https://archlinux.org/) based system you can install one from the `extra`
repository with `pacman -S redis` and the start it (the easy way at least) with
`systemctl start redis.service`.

[Redis-rs](https://docs.rs/redis/latest/redis/index.html) provides the Redis
interface and [serde](https://serde.rs/) provides the serializing and
deserializing of data to something (just JSON, using
[serde_json](https://docs.rs/serde_json/latest/serde_json/)) that we can store
in the Redis database.

To run it and store something (not quite gibberish but almost), simply run

Oh, wait. First make sure that you don't store anything valuable
under the `foo` key, because we're about to overwrite that!

Then,

``` sh
cargo run
```

and you _should_ see absolutely nothing. Which is good, as long as `$?` is zero.
