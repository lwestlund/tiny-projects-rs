use std::error::Error;

use redis::{Commands, ErrorKind, FromRedisValue, ToRedisArgs, Value};
use serde::{Deserialize, Serialize};

/// Enum data, similar to what prost would generate from a Protobuf enum.
#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq)]
#[repr(i32)]
enum E {
    E1 = 1,
    E2 = 2,
}
impl ToRedisArgs for E {
    fn write_redis_args<W>(&self, out: &mut W)
    where
        W: ?Sized + redis::RedisWrite,
    {
        let mut buf = itoa::Buffer::new();
        let s = buf.format(*self as i32);
        out.write_arg(s.as_bytes());
    }
}

/// The data structure that we want to store in the Redis database.
#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct S([Option<E>; 3]);
impl S {
    fn new() -> Self {
        Self([None, Some(E::E1), Some(E::E2)])
    }
}

/// Trait implementation used to convert the Rust data structure to something
/// that can be stored in Redis.
impl ToRedisArgs for S {
    fn write_redis_args<W>(&self, out: &mut W)
    where
        W: ?Sized + redis::RedisWrite,
    {
        out.write_arg(serde_json::to_string(self).unwrap().as_bytes());
    }
}

/// Trait implementation used to (try to) convert from something stored in Redis
/// to a Rust data structure.
impl FromRedisValue for S {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<Self> {
        match v {
            Value::Data(ref bytes) => Ok(serde_json::from_slice(bytes.as_slice())
                .map_err(|_| (ErrorKind::TypeError, "?"))?),
            _ => Err((ErrorKind::TypeError, "").into()),
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let client = redis::Client::open("redis://127.0.0.1")?;
    let mut connection = client.get_connection()?;

    let s1 = S::new();
    connection.set("foo", &s1)?;
    let s2: S = connection.get("foo")?;
    assert_eq!(s1, s2);

    Ok(())
}
