# Tiny projects - in Rust

A collection of projects (and maybe not much more than examples in some cases).

The inspiration to write these comes from things that I run into, both in
professional and personal capacity, and wonder how it works or how you could do
it in Rust.

# What's included 🔋

## [gR(s)PC](./grspc/README.md)

Server-client applications using gRPC to communicate.

Uses [tonic](https://docs.rs/tonic/latest/tonic/) for gRPC and
[protobuf](https://protobuf.dev/) for messages.

## [R(edi)s](./r-edi-s/README.md)

Redis storage from Rust.

Uses
[Redis-rs](https://docs.rs/redis/latest/redis/index.html) for the Redis
interface, [serde](https://serde.rs/) to do the heavy lifting in serializing and
deserializing data to something (just JSON, using
[serde_json](https://docs.rs/serde_json/latest/serde_json/)) that we can store
in the Redis database.
